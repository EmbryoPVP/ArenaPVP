package com.embryopvp.arenapvp.listeners;

import com.embryopvp.arenapvp.gui.*;
import com.embryopvp.arenapvp.handlers.PlayerHandler;
import com.embryopvp.arenapvp.kits.UHCKit;
import com.embryopvp.arenapvp.scoreboard.UHCFFAScoreboard;
import com.embryopvp.arenapvp.stats.DataLoader;
import com.embryopvp.arenapvp.stats.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;


public class WorldChange implements Listener {

    @EventHandler
    public void onChangeWorld(PlayerChangedWorldEvent e) {
        //is another player trying to pvp another player? If so don't make other players take damage
        Player p = e.getPlayer();

        if (p.getWorld() == Bukkit.getWorld("uhcffa")) {

            //set player gamemode
            p.setGameMode(GameMode.SURVIVAL);

            //load player stats
            PlayerData playerData = PlayerHandler.getInstance().getPlayerData(p);
            DataLoader.loadUHCFFAStats(playerData);

            //clear player inventory
            p.getInventory().clear();

            //give kit items and armor
            UHCKit.getInstance().giveArmor(p);
            UHCKit.getInstance().giveKitItems(p);


            //set scoreboard
            UHCFFAScoreboard.getInstance().addPlayerToScoreboard(p);
            UHCFFAScoreboard.getInstance().joinScoreboard(p);

            p.setHealth(20.0D);
            p.setFoodLevel(20);
        }

        if (p.getWorld() == Bukkit.getWorld("world")) {

            //clear player inventory
            p.getInventory().clear();

        }

        if (p.getWorld() == Bukkit.getWorld("lobby")) {

            //remove any scoreboards
            UHCFFAScoreboard.getInstance().removePlayerFromScoreboard(p);

            //clear player inventory
            p.getInventory().clear();

            //remove any armor the player might have had on
            p.getInventory().setArmorContents(null);
            p.getInventory().setHelmet(null);
            p.getInventory().setChestplate(null);
            p.getInventory().setLeggings(null);
            p.getInventory().setBoots(null);

            //give the players the gui menus they need
            KitCreationGUI.getInstance().giveKitCreationGUI(p);
            CreatePartyGUI.getInstance().giveCreatePartyGUI(p);
            Ranked1v1GUI.getInstance().giveRanked1v1GUI(p);
            UnrankedGUI.getInstance().giveUnrankedGUI(p);
            FFAGUI.getInstance().giveFFAGUI(p);
            EventsGUI.getInstance().giveEventsGUI(p);


        }
    }


}
