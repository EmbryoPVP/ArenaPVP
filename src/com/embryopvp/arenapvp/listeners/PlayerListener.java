package com.embryopvp.arenapvp.listeners;

import com.embryopvp.arenapvp.config.LobbyManager;
import com.embryopvp.arenapvp.gui.*;
import com.embryopvp.arenapvp.handlers.PlayerHandler;
import com.embryopvp.arenapvp.kits.UHCKit;
import com.embryopvp.arenapvp.scoreboard.UHCFFAScoreboard;
import com.embryopvp.arenapvp.stats.DataLoader;
import com.embryopvp.arenapvp.stats.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;


public class PlayerListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        //register our player that just joined
        PlayerHandler.getInstance().register(e.getPlayer());

        //clear the players inventory
        e.getPlayer().getInventory().clear();

        //remove any armor the player may have
        e.getPlayer().getInventory().setArmorContents(null);
        e.getPlayer().getInventory().setHelmet(null);
        e.getPlayer().getInventory().setChestplate(null);
        e.getPlayer().getInventory().setLeggings(null);
        e.getPlayer().getInventory().setBoots(null);

        //give the player the gui menus they need
        KitCreationGUI.getInstance().giveKitCreationGUI(e.getPlayer());
        Ranked1v1GUI.getInstance().giveRanked1v1GUI(e.getPlayer());
        CreatePartyGUI.getInstance().giveCreatePartyGUI(e.getPlayer());
        UnrankedGUI.getInstance().giveUnrankedGUI(e.getPlayer());
        FFAGUI.getInstance().giveFFAGUI(e.getPlayer());
        EventsGUI.getInstance().giveEventsGUI(e.getPlayer());

        //teleport the player to the lobby
        e.getPlayer().teleport(LobbyManager.getInstance().getLobbySpawn());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();

        PlayerData playerData = PlayerHandler.getInstance().getPlayerData(player);

        //save player data and unregister player
        //playerData.save();
        playerData.updateStats();
        PlayerHandler.getInstance().unregister(player);

    }


}
