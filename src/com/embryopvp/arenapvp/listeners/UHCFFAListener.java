package com.embryopvp.arenapvp.listeners;

import com.embryopvp.arenapvp.handlers.PlayerHandler;
import com.embryopvp.arenapvp.kits.UHCKit;
import com.embryopvp.arenapvp.scoreboard.UHCFFAScoreboard;
import com.embryopvp.arenapvp.stats.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerRespawnEvent;


public class UHCFFAListener implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        //is another player trying to pvp another player? If so don't make other players take damage
        Player p = e.getEntity();

        if (p.getWorld() == Bukkit.getWorld("uhcffa") && p.getKiller().getWorld() == Bukkit.getWorld("uhcffa")) {

            //update player stats
            PlayerData killedPlayerData = PlayerHandler.getInstance().getPlayerData(p);
            PlayerData playerKillerData = PlayerHandler.getInstance().getPlayerData(p.getKiller());

            killedPlayerData.updateUHCFFA_TotalDeaths();
            killedPlayerData.updateUHCFFA_CurrentDeaths();
            killedPlayerData.resetUHCFFA_KillStreak();
            killedPlayerData.updateUHCFFA_Stats(killedPlayerData.getName(), killedPlayerData.getUUID().toString());

            playerKillerData.updateUHCFFA_TotalKills();
            playerKillerData.updateUHCFFA_CurrentKills();
            playerKillerData.updateUHCFFA_KillStreak();
            playerKillerData.updateUHCFFA_Stats(playerKillerData.getName(), playerKillerData.getUUID().toString());

            //update player scoreboards
            UHCFFAScoreboard.getInstance().updateScoreboard(p);
            UHCFFAScoreboard.getInstance().updateScoreboard(p.getKiller());

            //give killer a golden apple
            UHCKit.getInstance().giveGoldenApple(p.getKiller());

        }

    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        Player p = e.getPlayer();

        if (p.getWorld() == Bukkit.getWorld("uhcffa")) {
            UHCKit.getInstance().giveArmor(p);
            UHCKit.getInstance().giveKitItems(p);
        }
    }

    @EventHandler
    public void onPlayerItemDrop(PlayerDropItemEvent e) {

        Player p = e.getPlayer();

        if (p.getWorld() == Bukkit.getWorld("uhcffa") && p.isDead()) {
            e.setCancelled(true);
        }

    }

}
