 package com.embryopvp.arenapvp.commands;

 import com.embryopvp.arenapvp.config.MessageManager;
 import com.embryopvp.arenapvp.config.MessageManager.PrefixType;
 import com.embryopvp.arenapvp.config.LobbyManager;
 import org.bukkit.entity.Player;
 import org.bukkit.ChatColor;
 import org.bukkit.Location;


 public class SetLobbyCommand implements SubCommand
 {

     public boolean onCommand(Player player, String[] args) {
         if (player.hasPermission(permission())) {
             try {
                 LobbyManager.getInstance().setLobbySpawn(player.getLocation());
                 MessageManager.getInstance().sendMessage(PrefixType.INFO, "The lobby spawn has been set successfully.", player);
                 return true;

             } catch (Exception e) {
                 e.printStackTrace();
                 MessageManager.getInstance().sendMessage(PrefixType.ERROR, "An unexpected error has occurred, check your logs for details.", player);
             }
         }
         return true;
     }


     @Override
     public String help(Player p) {
         return "use /ap setlobby to set the lobby for the uhc spawn";
     }

     @Override
     public String permission() {
         return "arenapvp.admin.setlobby";
     }

 }


