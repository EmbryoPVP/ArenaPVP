 package com.embryopvp.arenapvp.commands;

 import com.embryopvp.arenapvp.arena.Arena;
 import com.embryopvp.arenapvp.arena.ArenaList;
 import com.embryopvp.arenapvp.arena.ArenaManager;
 import com.embryopvp.arenapvp.config.MessageManager;
 import com.embryopvp.arenapvp.config.MessageManager.PrefixType;
 import com.embryopvp.arenapvp.exceptions.ArenaIDNotFoundException;
 import org.bukkit.ChatColor;
 import org.bukkit.configuration.file.FileConfiguration;
 import org.bukkit.entity.Player;

 import java.io.IOException;
 import java.util.Iterator;


 public class ArenaCommand implements SubCommand
 {

     public boolean onCommand(Player player, String[] args) {

         /*if (args[0].equalsIgnoreCase("create")) {

             if (args[1] != null) {

                 Arena arena = ArenaManager.getInstance().getArenaByName(args[1]);

                 if (arena != null) {
                     MessageManager.getInstance().sendMessage(PrefixType.ERROR, "That arena already exists.", player);
                     return false;
                 }

                 arena = new Arena(args[1]);
                 ArenaManager.getInstance().loadArena(arena.getName());
                 arena = ArenaManager.getInstance().getArenaByName(arena.getName());
                 MessageManager.getInstance().sendMessage(PrefixType.INFO, String.format("The arena %s has been created", args[1]), player);


             } else {
                 MessageManager.getInstance().sendMessage(PrefixType.ERROR, "to create an areana, use /ap arena create [Arena}", player);
             }

         }*/

         if (args[0].equalsIgnoreCase("setspawn")) {

             if (args[1] != null) {

                 if (args[1].equalsIgnoreCase("uhcffa")) {

                     try {
                         Object arenaObject = ArenaManager.getInstance().getArena("UHCFFA");

                         Arena uhcffa = (Arena) arenaObject;

                         uhcffa.getArenaConfig().set("arena.ffa-spawn.x", player.getLocation().getX());
                         uhcffa.getArenaConfig().set("arena.ffa-spawn.z", player.getLocation().getZ());
                         uhcffa.getArenaConfig().set("arena.ffa-spawn.y", player.getLocation().getY());

                         try {
                             uhcffa.getArenaConfig().save(uhcffa.getArenaFile());
                         } catch (IOException ex) {
                             ex.printStackTrace();
                             MessageManager.getInstance().sendPrefixMessage(PrefixType.ARENA, "Unable to set the arena spawn.", player);
                         }

                         MessageManager.getInstance().sendMessage(PrefixType.INFO, "The spawn for the " + uhcffa.getArenaID() + " arena has been set!", player);


                     } catch (ArenaIDNotFoundException e) {
                         e.printStackTrace();
                         MessageManager.getInstance().sendPrefixMessage(PrefixType.ARENA, "Unable to set the arena spawn.", player);
                     }
                 }


             } else {
                 MessageManager.getInstance().sendMessage(PrefixType.ERROR, "to set a spawn for an ffa arena, use /ap arena setspawn [Arena]", player);
             }

         }

         if (args[0].equalsIgnoreCase("list")) {
             Object arenasList = ArenaManager.getInstance().getArenas();
             MessageManager.getInstance().sendMessage(PrefixType.INFO, "Currently loaded Arenas: " + ((ArenaList)arenasList).size(), player );
             if (((ArenaList)arenasList).size() == 0) {
                 MessageManager.getInstance().sendMessage(PrefixType.ERROR, "No arenas that are loaded have been detected.", player);
             }
             Iterator arenaIterator = ((ArenaList)arenasList).iterator();
             while (arenaIterator.hasNext()) {
                 Arena localArena = (Arena)arenaIterator.next();
                 MessageManager.getInstance().sendMessage(PrefixType.INFO,
                         (localArena.isEnabled() ? ChatColor.GREEN + "ON " : new StringBuilder().append(ChatColor.RED).append("OFF ").toString()) + localArena.getArenaID(), player);
             }
             return true;
         }

         return true;
     }


     @Override
     public String help(Player p) {
         return "use /ap arena to manage arenas";
     }

     @Override
     public String permission() {
         return "arenapvp.arena.create";
     }

 }


