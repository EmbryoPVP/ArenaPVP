package com.embryopvp.arenapvp.stats;

import com.embryopvp.arenapvp.config.ConfigManager;
import com.embryopvp.arenapvp.database.DatabaseManager;
import com.embryopvp.arenapvp.database.DatabaseTask;
import com.embryopvp.arenapvp.database.DatabaseThread;
import com.microcraftmc.microdb.MicroDB;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class PlayerData {

    private Player player;
    private String name;
    private UUID UUID;
    private int uhcffa_TotalKills;
    private int uhcffa_TotalDeaths;
    private int uhcffa_currentKills;
    private int uhcffa_currentDeaths;
    private int uhcffa_killStreak;
    private boolean update = false;

    public PlayerData(Player player) {

        this.player = player;

        this.UUID = player.getUniqueId();
        this.name = player.getName();

        //ConfigManager.getInstance().loadPlayerData(this);
        //DataLoader.loadUHCFFAStats(this);

        getUHCFFA_Stats(name, UUID.toString());

    }

    public PlayerData(String playerName) {

        this.name = playerName;

        this.player = Bukkit.getPlayer(playerName);
        this.UUID = player.getUniqueId();

        //ConfigManager.getInstance().loadPlayerData(this);
        //DataLoader.loadUHCFFAStats(this);
        getUHCFFA_Stats(name, UUID.toString());

    }

    public PlayerData(String playerName, Boolean update) {
        this.name = playerName;

        if (update) {
            this.player = null;
            this.update = true;
        } else {
            this.player = Bukkit.getPlayer(playerName);
            this.UUID = player.getUniqueId();
        }

        //ConfigManager.getInstance().loadPlayerData(this);
        //DataLoader.loadUHCFFAStats(this);
        getUHCFFA_Stats(name, UUID.toString());

    }

    public void save() {
        ConfigManager.getInstance().savePlayerData(this);
    }

    public Player getPlayer() {
        return player;
    }

    public String getName(){
        return name;
    }

    public UUID getUUID() {
        return UUID;
    }

    public void setUHCFFA_TotalKills(int totalKills) {
        uhcffa_TotalKills = totalKills;
    }

    public void setUHCFFA_TotalDeaths(int totalDeaths) {
        uhcffa_TotalDeaths = totalDeaths;
    }

    public int getUHCFFA_TotalKills() {
        return uhcffa_TotalKills;
    }

    public int getUHCFFA_TotalDeaths() {
        return uhcffa_TotalDeaths;
    }

    public int getUHCFFA_CurrentKills() {
        return uhcffa_currentKills;
    }

    public int getUHCFFA_CurrentDeaths(){
        return uhcffa_currentDeaths;
    }

    public int getUHCFFA_KillStreak() {
        return uhcffa_killStreak;
    }

    public void updateUHCFFA_TotalKills() {
        uhcffa_TotalKills = uhcffa_TotalKills + 1;
    }

    public void updateUHCFFA_TotalDeaths(){
        uhcffa_TotalDeaths = uhcffa_TotalDeaths + 1;
    }

    public void updateUHCFFA_CurrentKills() {
        uhcffa_currentKills = uhcffa_currentKills + 1;
    }

    public void updateUHCFFA_CurrentDeaths() {
        uhcffa_currentDeaths = uhcffa_currentDeaths +1;
    }

    public void updateUHCFFA_KillStreak() {
        uhcffa_killStreak = uhcffa_killStreak + 1;
    }

    public void resetUHCFFA_CurrentKills() {
        uhcffa_currentKills = 0;
    }

    public void resetUHCFFA_CurrentDeaths() {
        uhcffa_currentDeaths = 0;
    }

    public void resetUHCFFA_KillStreak() {
        uhcffa_killStreak = 0;
    }

    public void updateStats() {
        DatabaseThread.addTask(new DatabaseTask("UPDATE `" + DatabaseManager.tablePrefix + "uhcffa_stats` SET `username`='" + name + "'," +
                "`kills`='" + getUHCFFA_TotalKills() + "',`deaths`='" + getUHCFFA_TotalDeaths() + "'," + "' WHERE uuid = '" + getUUID() + "'"));
    }

    public void getUHCFFA_Stats(String name, String uuid) {
        PreparedStatement statement = null;
        Connection con = null;
        try {

            if (MicroDB.getStorageManager().getStorageEngine().getConnection() != null) {
                con = MicroDB.getStorageManager().getStorageEngine().getConnection();
            } else {
                return;
            }
            statement = con.prepareStatement(UHCFFA_Data.getInstance().selectAllEntry);
            statement.setString(1, name);
            statement.setString(2, uuid);

            ResultSet rs = statement.executeQuery();

            if (!rs.next()) {
                statement.close();
                statement = con.prepareStatement(UHCFFA_Data.getInstance().insertEntry);
                statement.setString(1, name);
                statement.setString(2, uuid);

                //insert our user
                statement.executeUpdate();
                statement.close();
            } else {
                setUHCFFA_TotalDeaths(rs.getInt("deaths"));
                setUHCFFA_TotalKills(rs.getInt("kills"));
            }

            statement.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateUHCFFA_Stats(String name, String uuid) {
        PreparedStatement statement = null;
        Connection con = null;
        try {

            if (MicroDB.getStorageManager().getStorageEngine().getConnection() != null) {
                con = MicroDB.getStorageManager().getStorageEngine().getConnection();
            } else {
                return;
            }

            //update deaths
            statement = con.prepareStatement(UHCFFA_Data.getInstance().updateTotalDeaths);
            statement.setInt(1, getUHCFFA_TotalDeaths());
            statement.setString(2, name);
            statement.setString(3, uuid);
            statement.executeUpdate();
            statement.close();

            //update kills
            statement = con.prepareStatement(UHCFFA_Data.getInstance().updateTotalKills);
            statement.setInt(1, getUHCFFA_TotalKills());
            statement.setString(2, name);
            statement.setString(3, uuid);
            statement.executeUpdate();
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
