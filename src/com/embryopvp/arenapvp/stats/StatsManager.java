package com.embryopvp.arenapvp.stats;

import com.embryopvp.arenapvp.config.MessageManager;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class StatsManager {

    // OFFLINE
    protected static HashMap<String, UHCFFAStats> UHCFFAStatsData = new HashMap<>();
    private static int refreshTime = 60;

    public static void setStatistics(UHCFFAStats sd) {
        String name = sd.name.toLowerCase();
        if(UHCFFAStatsData.containsKey(name))
            UHCFFAStatsData.remove(name);
        UHCFFAStatsData.put(name, sd);
    }


}
