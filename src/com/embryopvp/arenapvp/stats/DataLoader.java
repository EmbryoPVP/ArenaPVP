package com.embryopvp.arenapvp.stats;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import com.embryopvp.arenapvp.config.MessageManager;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.embryopvp.arenapvp.database.DatabaseManager;
import com.embryopvp.arenapvp.database.DatabaseResponse;
import com.embryopvp.arenapvp.database.DatabaseTask;
import com.embryopvp.arenapvp.database.DatabaseThread;

public class DataLoader  {

    public static void loadUHCFFAStats(PlayerData playerData) {
        DatabaseTask task = new DatabaseTask("SELECT * FROM `" + DatabaseManager.tablePrefix + "uhcffa_stats` WHERE uuid='" + playerData.getUUID().toString() + "' AND username='" + playerData.getName() + "'", true, statsHandler);
        task.obj = new Object[] { playerData };
        DatabaseThread.addTask(task);
    }


    private static DatabaseResponse statsHandler = new StatsResponseHandler();

}

class StatsResponseHandler implements DatabaseResponse {

    @Override
    public void response(DatabaseTask task, ResultSet rs) {
        try {
            int kills = 0, deaths = 0;

            PlayerData playerData = null;
            String name = null;
            String uuid = null;


            playerData = (PlayerData) task.obj[0];

            if (playerData == null || playerData.getPlayer() == null) {
                return;
            }

            name = playerData.getName();
            uuid = playerData.getUUID().toString();

            if(rs.next()) {
                kills = rs.getInt("kills");
                deaths = rs.getInt("deaths");
                if (uuid.isEmpty()) {
                    uuid = rs.getString("uuid");
                }
            } else {

                DatabaseThread.addTask(new DatabaseTask("INSERT INTO `" + DatabaseManager.tablePrefix + "uhcffa_stats`(`uuid`, `username`) VALUES ('" + uuid + "','" + name + "')"));

            }

            PlayerData pd = new PlayerData(name);
            pd.setUHCFFA_TotalDeaths(deaths);
            pd.setUHCFFA_TotalKills(kills);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}