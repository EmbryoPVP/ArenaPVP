package com.embryopvp.arenapvp.stats;


import com.embryopvp.arenapvp.config.MessageManager;
import com.embryopvp.arenapvp.database.DatabaseManager;
import com.embryopvp.arenapvp.database.DatabaseResponse;
import com.embryopvp.arenapvp.database.DatabaseTask;
import com.embryopvp.arenapvp.database.DatabaseThread;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UHCFFAStats {

    protected long creation = System.currentTimeMillis();
    String name, uuid;
    int kills, deaths;

    public UHCFFAStats(String name, String uuid, int kills, int deaths) {
        this.name = name;
        this.uuid = uuid;
        this.kills = kills;
        this.deaths = deaths;
    }

    public void updateStats() {
        DatabaseThread.addTask(new DatabaseTask("UPDATE `" + DatabaseManager.tablePrefix + "uhcffa_stats` SET `username`='" + name + "'," +
                "`kills`='" + kills + "',`deaths`='" + deaths + "'," + "' WHERE uuid = '" + uuid + "'"));
    }

    // VALUES

    public int getKills() {
        return kills;
    }

    public void addKill() {
        kills++;
    }

    public int getDeaths() {
        return deaths;
    }

    public void addDeath() {
        deaths++;
    }


}




