package com.embryopvp.arenapvp.stats;

import com.microcraftmc.microdb.MicroDB;
import com.microcraftmc.microdb.storage.database.DatabaseTable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UHCFFA_Data extends DatabaseTable {

    private static UHCFFA_Data instance = new UHCFFA_Data();

    public UHCFFA_Data() {

    }

    public static UHCFFA_Data getInstance() {
        return instance;
    }

    @Override
    public String tableName() {
        return "uhcffa_stats";
    }

    public final String selectEntryName = "SELECT * FROM " + MicroDB.getStorageManager().getStorageEngine().tablePrefix() + tableName() + " WHERE username=? AND uuid=?";

    public final String selectEntryUuid = "SELECT * FROM " + MicroDB.getStorageManager().getStorageEngine().tablePrefix() + tableName() + " WHERE uuid=?";

    public final String selectAllEntry = "SELECT * FROM " + MicroDB.getStorageManager().getStorageEngine().tablePrefix()  + tableName() + " WHERE username=? AND uuid=?";

    public final String insertEntry = "INSERT INTO " + MicroDB.getStorageManager().getStorageEngine().tablePrefix() + tableName() + "(username,uuid) VALUES(?,?)";

    public final String deleteEntry = "DELETE FROM " + MicroDB.getStorageManager().getStorageEngine().tablePrefix() + tableName() + " WHERE username=? AND uuid=?";

    public final String updateNameByUuid = "UPDATE "+ MicroDB.getStorageManager().getStorageEngine().tablePrefix() + tableName() + " SET username=? WHERE uuid=?";

    public final String updateUuidByName = "UPDATE "+ MicroDB.getStorageManager().getStorageEngine().tablePrefix() + tableName() +" SET uuid=? WHERE name=?";

    public final String getTotalDeaths = "SELECT deaths FROM " + MicroDB.getStorageManager().getStorageEngine().tablePrefix() + tableName() +" WHERE username=? AND uuid=?";

    public final String getTotalKills = "SELECT kills FROM " + MicroDB.getStorageManager().getStorageEngine().tablePrefix() + tableName() +" WHERE username=? AND uuid=?";

    public final String updateTotalDeaths = "UPDATE "+ MicroDB.getStorageManager().getStorageEngine().tablePrefix() + tableName() + " SET deaths=? WHERE username=? AND uuid=?";

    public final String updateTotalKills = "UPDATE "+ MicroDB.getStorageManager().getStorageEngine().tablePrefix() + tableName() + " SET kills=? WHERE username=? AND uuid=?";



}
