package com.embryopvp.arenapvp.scoreboard;

import com.embryopvp.arenapvp.handlers.PlayerHandler;
import com.embryopvp.arenapvp.stats.DataLoader;
import com.embryopvp.arenapvp.stats.PlayerData;
import com.google.common.collect.Maps;

import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class UHCFFAScoreboard {

    private static UHCFFAScoreboard instance = new UHCFFAScoreboard();

    private Map<String, Scoreboard> scoreboards = Maps.newHashMap();

    public UHCFFAScoreboard() {

    }

    public static UHCFFAScoreboard getInstance() {
        return instance;
    }

    public void addPlayerToScoreboard(Player p) {
        Scoreboard scoreboard = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("uhcffa", "dummy");

        //UHC FFA Scoreboard display settings
        objective.setDisplayName(ChatColor.DARK_GREEN + "UHC FFA");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        scoreboards.put(p.getName(), scoreboard);
    }

    public Map<String, Scoreboard> getScoreboards() {
        return scoreboards;
    }

    public void joinScoreboard(Player p) {
        updateTotalKills(p);
        updateTotalDeaths(p);
        updateCurrentDeaths(p);
        updateCurrentKills(p);
        updateKillStreak(p);
        p.setScoreboard(getScoreboards().get(p.getName()));
    }

    public void updateScoreboard(Player p) {
        updateTotalKills(p);
        updateTotalDeaths(p);
        updateCurrentDeaths(p);
        updateCurrentKills(p);
        updateKillStreak(p);
        p.setScoreboard(getScoreboards().get(p.getName()));
    }

    public void removePlayerFromScoreboard(Player p) {
        getScoreboards().get(p.getName()).clearSlot(DisplaySlot.SIDEBAR);
    }

    public void shutdown() {
        for (Scoreboard scoreboard : this.scoreboards.values()) {
            for (Objective objective : scoreboard.getObjectives()) {
                objective.unregister();
            }
            scoreboard.clearSlot(DisplaySlot.SIDEBAR);
        }
    }

    public void updateTotalKills(Player p) {
        Objective objective = getScoreboards().get(p.getName()).getObjective("uhcffa");
        if (objective != null) {
            PlayerData playerData = PlayerHandler.getInstance().getPlayerData(p);
            objective.getScore(ChatColor.GOLD + "Total Kills").setScore(playerData.getUHCFFA_TotalKills());
        }
    }

    public void updateTotalDeaths(Player p) {
        Objective objective = getScoreboards().get(p.getName()).getObjective("uhcffa");
        if (objective != null) {
            PlayerData playerData = PlayerHandler.getInstance().getPlayerData(p);
            objective.getScore(ChatColor.GOLD + "Total Deaths").setScore(playerData.getUHCFFA_TotalDeaths());
        }
    }

    public void updateCurrentDeaths(Player p) {
        Objective objective = getScoreboards().get(p.getName()).getObjective("uhcffa");
        if (objective != null) {
            PlayerData playerData = PlayerHandler.getInstance().getPlayerData(p);
            objective.getScore(ChatColor.GOLD + "Current Deaths").setScore(playerData.getUHCFFA_CurrentDeaths());
        }
    }

    public void updateCurrentKills(Player p) {
        Objective objective = getScoreboards().get(p.getName()).getObjective("uhcffa");
        if (objective != null) {
            PlayerData playerData = PlayerHandler.getInstance().getPlayerData(p);
            objective.getScore(ChatColor.GOLD + "Current Kills").setScore(playerData.getUHCFFA_CurrentKills());
        }
    }

    public void updateKillStreak(Player p) {
        Objective objective = getScoreboards().get(p.getName()).getObjective("uhcffa");
        if (objective != null) {
            PlayerData playerData = PlayerHandler.getInstance().getPlayerData(p);
            objective.getScore(ChatColor.GOLD + "Kill Streak").setScore(playerData.getUHCFFA_KillStreak());
        }
    }


}
