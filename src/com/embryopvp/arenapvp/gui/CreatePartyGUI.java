package com.embryopvp.arenapvp.gui;


import com.embryopvp.arenapvp.arena.Arena;
import com.embryopvp.arenapvp.arena.ArenaManager;
import com.embryopvp.arenapvp.config.MessageManager;
import com.embryopvp.arenapvp.config.MessageManager.PrefixType;
import com.embryopvp.arenapvp.exceptions.ArenaIDNotFoundException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import static org.bukkit.ChatColor.GOLD;
import static org.bukkit.ChatColor.GREEN;

public class CreatePartyGUI implements Listener {

    private static CreatePartyGUI instance = new CreatePartyGUI();

    public CreatePartyGUI() {

    }

    public static CreatePartyGUI getInstance() {
        return instance;
    }

    private void openGUI(Player player){
        Inventory inv = Bukkit.createInventory(null, 18, GOLD + "FFA");

        ItemStack soupFFA = new ItemStack(Material.MUSHROOM_SOUP, 1);
        ItemMeta soupFFAMeta = soupFFA.getItemMeta();

        soupFFAMeta.setDisplayName(GREEN + "Soup FFA");
        soupFFA.setItemMeta(soupFFAMeta);

        ItemStack kohiFFA = new ItemStack(Material.POTION, 1);
        ItemMeta kohiFFAMeta = kohiFFA.getItemMeta();

        kohiFFAMeta.setDisplayName(GREEN + "Kohi FFA");
        kohiFFA.setItemMeta(kohiFFAMeta);

        ItemStack mcsgFFA = new ItemStack(Material.FISHING_ROD, 1);
        ItemMeta mcsgFFAMeta = mcsgFFA.getItemMeta();

        mcsgFFAMeta.setDisplayName(GREEN + "MCSG FFA");
        mcsgFFA.setItemMeta(mcsgFFAMeta);

        ItemStack uhcFFA = new ItemStack(Material.GOLDEN_APPLE, 1);
        ItemMeta uhcFFAMeta = uhcFFA.getItemMeta();

        uhcFFAMeta.setDisplayName(GREEN + "UHC FFA");
        uhcFFA.setItemMeta(uhcFFAMeta);

        ItemStack closeClay = new ItemStack(Material.STAINED_CLAY, 1, (short) 14);
        ItemMeta closeClayMeta = closeClay.getItemMeta();

        closeClayMeta.setDisplayName(GREEN + "Close");
        closeClay.setItemMeta(closeClayMeta);

        inv.setItem(0, soupFFA);
        inv.setItem(1, kohiFFA);
        inv.setItem(2, mcsgFFA);
        inv.setItem(3, uhcFFA);
        inv.setItem(17, closeClay);

        player.openInventory(inv);
    }

    public void giveCreatePartyGUI(Player p) {
        ItemStack createParty = new ItemStack(Material.NAME_TAG);
        ItemMeta createPartyMeta = createParty.getItemMeta();
        createPartyMeta.setDisplayName(GREEN + "Create Party");
        createParty.setItemMeta(createPartyMeta);

        p.getInventory().setItem(2, createParty);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if(!ChatColor.stripColor(event.getInventory().getName()).equalsIgnoreCase("FFA")) {
            return;
        }

        Player player = (Player) event.getWhoClicked();
        event.setCancelled(true);

        if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR || !event.getCurrentItem().hasItemMeta()) {
            player.closeInventory();
            return;
        }

        if (ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Soup FFA")) {
            player.closeInventory();
            MessageManager.getInstance().sendPrefixMessage(PrefixType.ARENA, "Soup FFA is coming soon!", player);
        }

        if (ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Kohi FFA")) {
            player.closeInventory();
            MessageManager.getInstance().sendPrefixMessage(PrefixType.ARENA, "Kohi FFA is coming soon!", player);
        }

        if (ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("MCSG FFA")) {
            player.closeInventory();
            MessageManager.getInstance().sendPrefixMessage(PrefixType.ARENA, "MCSG FFA is coming soon!", player);
        }

        if (ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("UHC FFA")) {

            try {
                Object arenaObject = ArenaManager.getInstance().getArena("UHCFFA");

                Arena uhcffa = (Arena) arenaObject;

                player.closeInventory();
                player.teleport(uhcffa.getFFASpawn());
                MessageManager.getInstance().sendPrefixMessage(PrefixType.ARENA, "You have been sent to the UHC FFA arena!", player);


            } catch (ArenaIDNotFoundException e) {
                e.printStackTrace();
                MessageManager.getInstance().sendPrefixMessage(PrefixType.ARENA, "Could not load UHC FFA arena, try again in a few minutes as it might be down for maintenance.", player);
            }
        }

        if (ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Close")) {
            player.closeInventory();
        }

    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Action a = event.getAction();
        ItemStack is = event.getItem();

        if (a == Action.PHYSICAL || is == null || is.getType() == Material.AIR) {
            return;
        }

        if (ChatColor.stripColor(is.getItemMeta().getDisplayName()).equalsIgnoreCase("Create Party")) {
            MessageManager.getInstance().sendMessage(PrefixType.ERROR, "Party feature not implemented yet.", event.getPlayer());
        }


    }



}
