package com.embryopvp.arenapvp;

import com.embryopvp.arenapvp.arena.ArenaManager;
import com.embryopvp.arenapvp.commands.PingCommand;
import com.embryopvp.arenapvp.commands.WhitelistCommand;
import com.embryopvp.arenapvp.config.CommandManager;
import com.embryopvp.arenapvp.config.ConfigManager;
import com.embryopvp.arenapvp.config.MessageManager;
import com.embryopvp.arenapvp.config.LobbyManager;
import com.embryopvp.arenapvp.database.DatabaseManager;
import com.embryopvp.arenapvp.gui.*;
import com.embryopvp.arenapvp.handlers.PlayerHandler;
import com.embryopvp.arenapvp.listeners.PlayerListener;
import com.embryopvp.arenapvp.listeners.UHCFFAListener;
import com.embryopvp.arenapvp.listeners.WorldChange;
import com.embryopvp.arenapvp.utils.LogUtil;
import com.embryopvp.arenapvp.utils.PingUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class ArenaPVP extends JavaPlugin
 {
   private static ArenaPVP p;
   public static boolean Snapshot = false;

   public void onEnable() {
        p = this;
        if (getDescription().getVersion().toLowerCase().contains("snapshot")) {
           Snapshot = true;
        }

        //attempt to load our config
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        ConfigManager.getInstance().setup();
        MessageManager.getInstance().setup();
        LobbyManager.getInstance().setup();
        PingUtil.getInstance().setup();
        ArenaManager.getInstance().setup();

        registerListener(Bukkit.getPluginManager());
        registerCommands();

        //load our database
       DatabaseManager.getInstance().open();
       DatabaseManager.getInstance().setupDatabase();


       //load player data
       PlayerHandler.getInstance().setup();

        logStartup();


   }

   public void onDisable() {
        PlayerHandler.getInstance().shutdown();
        getLogger().info(getDescription().getName() + " has been disabled!");
  }

   public static ArenaPVP getPlugin() {
       return p;
   }

   private void logStartup() {

       LogUtil.LogToConsole("");
       LogUtil.LogToConsole(ChatColor.BLUE + "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
       LogUtil.LogToConsole("");
       LogUtil.LogToConsole(ChatColor.AQUA + "ArenaPVP");
       LogUtil.LogToConsole(ChatColor.AQUA + "Version " + getDescription().getVersion());
       LogUtil.LogToConsole("");
       LogUtil.LogToConsole(ChatColor.BLUE + "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
       LogUtil.LogToConsole("");

   }


  private void registerListener(PluginManager pluginManager) {

      //final PlayerChat playerChat = new PlayerChat();
      final WhitelistGUI whitelistGUI = new WhitelistGUI();
      final WhitelistRemovePlayersGUI whitelistRemovePlayersGUI = new WhitelistRemovePlayersGUI();
      final WorldChange worldChange = new WorldChange();
      final UHCFFAListener uhcffaListener = new UHCFFAListener();
      final PlayerListener playerListener = new PlayerListener();

      //register the gui menus
      final FFAGUI ffaGUI = new FFAGUI();
      final KitCreationGUI kitCreationGUI = new KitCreationGUI();
      final Ranked1v1GUI ranked1v1GUI = new Ranked1v1GUI();
      final CreatePartyGUI createPartyGUI = new CreatePartyGUI();
      final UnrankedGUI unrankedGUI = new UnrankedGUI();
      final EventsGUI eventsGUI = new EventsGUI();

      //register the events
      //pluginManager.registerEvents(playerChat, this);

      //register gui listeners
      pluginManager.registerEvents(whitelistGUI, this);
      pluginManager.registerEvents(whitelistRemovePlayersGUI, this);
      pluginManager.registerEvents(worldChange, this);
      pluginManager.registerEvents(uhcffaListener, this);
      pluginManager.registerEvents(playerListener, this);
      pluginManager.registerEvents(ffaGUI, this);
      pluginManager.registerEvents(kitCreationGUI, this);
      pluginManager.registerEvents(ranked1v1GUI, this);
      pluginManager.registerEvents(createPartyGUI, this);
      pluginManager.registerEvents(unrankedGUI, this);
      pluginManager.registerEvents(eventsGUI, this);

  }

  private void registerCommands() {
        getCommand("arenapvp").setExecutor(new CommandManager());
        getCommand("wl").setExecutor(new WhitelistCommand());
        getCommand("ping").setExecutor(new PingCommand());
  }


 }


