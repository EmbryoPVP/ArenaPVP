package com.embryopvp.arenapvp.config;

import com.embryopvp.arenapvp.ArenaPVP;
import com.embryopvp.arenapvp.stats.PlayerData;
import com.embryopvp.arenapvp.utils.LogUtil;

import com.mysql.jdbc.log.Log;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import javax.annotation.Nonnull;
import java.io.*;


public class ConfigManager {

    private static ConfigManager instance = new ConfigManager();
    private static Plugin p = ArenaPVP.getPlugin();

    private FileConfiguration lobby;
    private FileConfiguration messages;
    private FileConfiguration scoreboards;

    private File lobbyFile;
    private File messagesFile;
    private File scoreboardsFile;

    public ConfigManager() {

    }

    public static ConfigManager getInstance() {
        return instance;
    }

    public void setup() {

        //load the default config
        p.getConfig().options().copyDefaults(true);
        p.saveDefaultConfig();

        //load our config files
        lobbyFile = new File(p.getDataFolder(), "lobby.yml");
        messagesFile = new File(p.getDataFolder(), "messages.yml");
        scoreboardsFile = new File(p.getDataFolder(), "scoreboards.yml");

        try {
            if(!lobbyFile.exists()) {
                loadFile("lobby.yml");
            }
            if(!messagesFile.exists()) {
                loadFile("messages.yml");
            }
            if (!scoreboardsFile.exists()) {
                loadFile("scoreboards.yml");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //load our config files
        lobby = YamlConfiguration.loadConfiguration(lobbyFile);
        messages = YamlConfiguration.loadConfiguration(messagesFile);
        scoreboards = YamlConfiguration.loadConfiguration(scoreboardsFile);


    }


    public FileConfiguration getConfig() {
        return p.getConfig();
    }

    public FileConfiguration getLobbyConfig() {
        return lobby;
    }

    public FileConfiguration getMessagesConfig() {
        return messages;
    }

    public FileConfiguration getScoreboardsConfig() {
        return scoreboards;
    }

    public FileConfiguration getArenaConfig(@Nonnull String name) {

        FileConfiguration arenaConfig;
        File arenaFile;

        arenaFile = new File(p.getDataFolder() + "/arenas/", name + ".yml");

        if (!arenaFile.exists()) {
            try {
                if (arenaFile.createNewFile()) {
                    LogUtil.LogToConsole("The arena " + name + "has been created!");

                } else {
                    LogUtil.LogToConsole("The arena file could not be created");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        arenaConfig = YamlConfiguration.loadConfiguration(arenaFile);
        return arenaConfig;
    }


    public boolean isDebugEnabled() {
        return p.getConfig().getBoolean("debug");
    }

    public void loadFile(String file)
    {
        File t = new File(p.getDataFolder(), file);
        LogUtil.LogToConsole("Writing new file: " + t.getAbsolutePath());

        try {
            t.createNewFile();
            FileWriter out = new FileWriter(t);
            if (!isDebugEnabled()) {
                //should we print the file we are writing for debug purposes?
                System.out.println(file);
            }
            //InputStream is = getClass().getResourceAsStream("/"+file);
            InputStream is = p.getResource(file);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                out.write(line + "\n");
                if (!isDebugEnabled()) {
                    System.out.print(line);
                }
            }
            out.flush();
            is.close();
            isr.close();
            br.close();
            out.close();

            if (!isDebugEnabled()) {
                LogUtil.LogToConsole("Loaded Config " + file + " successfully!");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void loadAreanaDir() {
        File arenaDir = new File(p.getDataFolder() + "/arenas/");

        if (!arenaDir.exists()) {
            try {
                if (arenaDir.mkdir()) {
                    LogUtil.LogToConsole("Arena directory created.");
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }

    }

    public void loadPlayerData(@Nonnull PlayerData player) {

        try {
            File playerDataDirectory = new File(p.getDataFolder(), "player_data");

            if ((!playerDataDirectory.exists()) && (!playerDataDirectory.mkdir())) {
                //could not load player data
                LogUtil.LogToConsole("Failed to load player " + player + ": Could not create player_data directory");
            }

            File playerFile = new File(playerDataDirectory, player + ".yml");

            if ((!playerFile.exists()) && (!playerFile.createNewFile())) {
                //could not load player data
                LogUtil.LogToConsole("Failed to load player " + player.getName() + ": Could not create player file.");
            }

            YamlConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);

            //set player uhc ffa stats
            player.setUHCFFA_TotalKills(playerConfig.getInt("uhcffa-stats.total-kills"));
            player.setUHCFFA_TotalDeaths(playerConfig.getInt("uhcffa-stats.total-deaths"));
            player.resetUHCFFA_KillStreak();
            player.resetUHCFFA_CurrentKills();
            player.resetUHCFFA_CurrentDeaths();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void savePlayerData(@Nonnull PlayerData player) {

        try {
            File playerDataDirectory = new File(p.getDataFolder(), "player_data");

            if ((!playerDataDirectory.exists()) && (!playerDataDirectory.mkdir())) {
                //could not load player data
                LogUtil.LogToConsole("Failed to load player " + player + ": Could not create player_data directory");
            }

            File playerFile = new File(playerDataDirectory, player.getName() + ".yml");

            if ((!playerFile.exists()) && (!playerFile.createNewFile())) {
                //could not load player data
                LogUtil.LogToConsole("Failed to load player " + player + ": Could not create player file.");
            }

            YamlConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);

            //set player uhc ffa stats
            playerConfig.set("uhcffa-stats.total-kills", player.getUHCFFA_TotalKills());
            playerConfig.set("uhcffa-stats.total-deaths", player.getUHCFFA_TotalDeaths());
            playerConfig.save(playerFile);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void saveConfig() {
        p.saveConfig();
    }

    public void saveLobbyConfig() {
        try {

            //save our lobby config file
            lobby.save(lobbyFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveMessagesConfig() {
        try {

            //save our messages config file
            messages.save(messagesFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveScoreboardsConfig() {
        try {
            //save our scoreboards config file
            scoreboards.save(scoreboardsFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveArenaConfig(@Nonnull String name) {

        FileConfiguration arenaConfig;
        File arenaFile;

        arenaFile = new File(p.getDataFolder() + "/arenas/", name + ".yml");

        if (!arenaFile.exists()) {
            try {
                if (arenaFile.createNewFile()) {
                    LogUtil.LogToConsole("The arena " + name + "has been created!");

                } else {
                    LogUtil.LogToConsole("The arena file could not be created");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        arenaConfig = YamlConfiguration.loadConfiguration(arenaFile);

        try {
            arenaConfig.save(arenaFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
