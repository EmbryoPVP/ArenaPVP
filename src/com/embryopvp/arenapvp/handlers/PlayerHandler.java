package com.embryopvp.arenapvp.handlers;

import com.embryopvp.arenapvp.config.ConfigManager;
import com.embryopvp.arenapvp.stats.PlayerData;
import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Map;

public class PlayerHandler {

    private static PlayerHandler instance = new PlayerHandler();
    private Map<Player, PlayerData> playerRegistry = Maps.newHashMap();


    public PlayerHandler() {

    }

    public static PlayerHandler getInstance() {
        return instance;
    }

    public PlayerData register(@Nonnull Player player) {

        PlayerData playerData = null;

        if (!playerRegistry.containsKey(player)) {
            playerData = new PlayerData(player);
            playerRegistry.put(player, playerData);
        }

        return playerData;

    }

    public PlayerData unregister(@Nonnull Player player) {
        return playerRegistry.remove(player);
    }

    public PlayerData getPlayerData(@Nonnull Player player) {
        return playerRegistry.get(player);
    }

    public Collection<PlayerData> getAll() {
        return playerRegistry.values();
    }

    public void setup() {

        for (Player player : Bukkit.getOnlinePlayers()) {
            register(player);
        }

    }

    public void shutdown() {
        for (PlayerData playerData : playerRegistry.values()) {
            ConfigManager.getInstance().savePlayerData(playerData);
        }

        playerRegistry.clear();
    }

}
