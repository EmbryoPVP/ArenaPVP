package com.embryopvp.arenapvp.kits;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

public class UHCKit {

    private static UHCKit instance = new UHCKit();

    public UHCKit() {

    }

    public static UHCKit getInstance() {
        return instance;
    }

    public void giveArmor(Player p) {

        //helmet
        ItemStack helmet = new ItemStack(Material.IRON_HELMET, 1);

        helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);


        //chest plate
        ItemStack chestPlate = new ItemStack(Material.IRON_CHESTPLATE, 1);

        chestPlate.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 2);

        //leggings
        ItemStack leggings = new ItemStack(Material.IRON_LEGGINGS, 1);

        leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);

        //boots
        ItemStack boots = new ItemStack(Material.IRON_BOOTS, 1);

        boots.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 2);

        //add armor to player
        p.getInventory().setHelmet(helmet);
        p.getInventory().setChestplate(chestPlate);
        p.getInventory().setLeggings(leggings);
        p.getInventory().setBoots(boots);

    }

    public void giveKitItems(Player p) {

        //sword
        ItemStack sword = new ItemStack(Material.IRON_SWORD, 1);

        sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);

        //bow
        ItemStack bow = new ItemStack(Material.BOW, 1);

        bow.addEnchantment(Enchantment.ARROW_INFINITE, 1);
        bow.addEnchantment(Enchantment.ARROW_DAMAGE, 1);

        //steak
        ItemStack steak = new ItemStack(Material.COOKED_BEEF, 16);


        //gold apples
        ItemStack goldApple = new ItemStack(Material.GOLDEN_APPLE, 1);


        //arrow
        ItemStack arrow = new ItemStack(Material.ARROW, 1);

        //add kit items to players inventory
        p.getInventory().setItem(0, sword);
        p.getInventory().setItem(1, bow);
        p.getInventory().setItem(2, steak);
        p.getInventory().setItem(3, goldApple);
        p.getInventory().setItem(4, goldApple);
        p.getInventory().setItem(8, arrow);

    }

    public void giveGoldenApple(Player p) {

        //golden head recipe
        ItemStack goldApple = new ItemStack(Material.GOLDEN_APPLE, 1);

        p.getInventory().setItem(5, goldApple);

    }

}
