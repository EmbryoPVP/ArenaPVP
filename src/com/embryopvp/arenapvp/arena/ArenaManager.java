package com.embryopvp.arenapvp.arena;

import com.avaje.ebeaninternal.server.cluster.mcast.Message;
import com.embryopvp.arenapvp.ArenaPVP;
import com.embryopvp.arenapvp.arena.core.UHCFFA;
import com.embryopvp.arenapvp.config.ConfigManager;
import com.embryopvp.arenapvp.config.MessageManager;
import com.embryopvp.arenapvp.exceptions.ArenaIDConflictException;
import com.embryopvp.arenapvp.exceptions.ArenaIDNotFoundException;
import com.embryopvp.arenapvp.exceptions.InvalidArenaIDException;
import com.embryopvp.arenapvp.utils.LogUtil;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import javax.annotation.Nonnull;
import javax.security.auth.login.Configuration;
import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArenaManager {

    private static ArenaManager instance = new ArenaManager();
    private static ArenaPVP p = ArenaPVP.getPlugin();
    private static ArenaList arenasList = new ArenaList();
    private static final Pattern name_pattern = Pattern.compile("^[\\w]++$");

    private static Map<String, Arena> ARENAS = new HashMap<String, Arena>();

    //Areana FileConfigs
    private FileConfiguration UHCFFA_Arena;

    //Arena Files
    private File arenaDir;
    private File UHCFFA_ArenaFile;


    public ArenaManager() {

    }

    public static ArenaManager getInstance() {
        return instance;
    }

    public void setup() {
        ArrayList setupList = new ArrayList();

        //make sure our arena directory exists
        loadAreanaDir();

        UHCFFA_ArenaFile = new File(arenaDir, "uhcffa.yml");


        try {
            if (!UHCFFA_ArenaFile.exists()) {
                loadArenaFile("uhcffa.yml");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //load our arena configs
        UHCFFA_Arena = YamlConfiguration.loadConfiguration(UHCFFA_ArenaFile);


        //now load our arenas
        setupList.add(new UHCFFA(UHCFFA_Arena.getBoolean("arena.enabled"), UHCFFA_Arena, UHCFFA_ArenaFile));


        Iterator setupIterator = setupList.iterator();
        while (setupIterator.hasNext()) {
            Arena localArena = (Arena)setupIterator.next();
            try {
                addArena(localArena);
                LogUtil.LogToConsole("Loaded Arena: " + localArena.getArenaID());
            } catch (Exception localException) {
                LogUtil.LogToConsole("Failed to load arena " + (localArena == null ? "null" : localArena.getArenaID()));
                localException.printStackTrace();
            }
        }


    }

    public void loadAreanaDir() {
        arenaDir = new File(p.getDataFolder() + "/arenas/");

        if (!arenaDir.exists()) {
            try {
                if (arenaDir.mkdir()) {
                    LogUtil.LogToConsole("Arena directory created.");
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }

    }


    public void loadArenaFile(String file)
    {
        File t = new File(arenaDir, file);
        LogUtil.LogToConsole("Writing new file: " + t.getAbsolutePath());

        try {
            t.createNewFile();
            FileWriter out = new FileWriter(t);
            if (!ConfigManager.getInstance().isDebugEnabled()) {
                //should we print the file we are writing for debug purposes?
                System.out.println(file);
            }
            //InputStream is = getClass().getResourceAsStream("/"+file);
            InputStream is = p.getResource(file);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                out.write(line + "\n");
                if (!ConfigManager.getInstance().isDebugEnabled()) {
                    System.out.print(line);
                }
            }
            out.flush();
            is.close();
            isr.close();
            br.close();
            out.close();

            if (!ConfigManager.getInstance().isDebugEnabled()) {
                LogUtil.LogToConsole("Loaded Config " + file + " successfully!");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void addArena(Arena paramArena) throws ArenaIDConflictException, InvalidArenaIDException
    {
        Matcher localMatcher = name_pattern.matcher(paramArena.getArenaID());
        if (!localMatcher.matches()) {
            throw new InvalidArenaIDException();
        }
        Iterator localIterator = arenasList.iterator();
        while (localIterator.hasNext())
        {
            Arena localArena = (Arena)localIterator.next();
            if (localArena.getArenaID().equals(paramArena.getArenaID())) {
                throw new ArenaIDConflictException();
            }
        }

        arenasList.add(paramArena);

        if (paramArena.isEnabled()) {
            paramArena.enableArena();
        } else {
            paramArena.disableArena();
        }

    }

    public boolean isEnabled(String paramString) throws ArenaIDNotFoundException
    {
        Iterator localIterator = arenasList.iterator();

        while (localIterator.hasNext())
        {
            Arena localArena = (Arena)localIterator.next();
            if (localArena.getArenaID().equals(paramString)) {
                return localArena.isEnabled();
            }
        }
        throw new ArenaIDNotFoundException();
    }

    public Arena getArena(String paramString) throws ArenaIDNotFoundException
    {
        Iterator localIterator = arenasList.iterator();

        while (localIterator.hasNext())
        {
            Arena localArena = (Arena)localIterator.next();
            if (localArena.getArenaID().equals(paramString)) {
                return localArena;
            }
        }
        throw new ArenaIDNotFoundException();
    }

    public ArenaList getArenas()
    {
        return arenasList;
    }

    public List<String> getArenaNames()
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = arenasList.iterator();

        while (localIterator.hasNext())
        {
            Arena localArena = (Arena)localIterator.next();
            localArrayList.add(localArena.getArenaID());
        }
        return localArrayList;
    }

    public FileConfiguration getArenaConfig(String paramString) throws ArenaIDNotFoundException
    {
        Iterator localIterator = arenasList.iterator();

        while (localIterator.hasNext())
        {
            Arena localArena = (Arena)localIterator.next();
            if (localArena.getArenaID().equals(paramString)) {
                return localArena.getArenaConfig();
            }
        }
        throw new ArenaIDNotFoundException();
    }

    public void saveArenaConfig(String paramString) throws ArenaIDNotFoundException
    {
        Iterator localIterator = arenasList.iterator();

        while (localIterator.hasNext())
        {
            Arena localArena = (Arena)localIterator.next();
            if (localArena.getArenaID().equals(paramString)) {
                try {
                    localArena.getArenaConfig().save(localArena.getArenaFile());
                } catch (IOException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
        throw new ArenaIDNotFoundException();
    }
}
