package com.embryopvp.arenapvp.arena.core;

import com.embryopvp.arenapvp.arena.Arena;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class UHCFFA extends Arena {

    private FileConfiguration arenaConfig;

    //Arena Files
    private File arenaDir;
    private File arenaFile;

    public UHCFFA(boolean enabled, FileConfiguration arenaConfig, File arenaFile) {
        super(enabled);

        setArenaID("UHCFFA");
        setName("UHC FFA");

        setArenaConfig(arenaConfig);
        setArenaFile(arenaFile);
        setWorld(arenaConfig.getString("arena.world"));
    }


    public void enableArena() {

    }

    public void disableArena() {

    }

}
