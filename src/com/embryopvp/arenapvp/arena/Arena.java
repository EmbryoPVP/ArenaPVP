package com.embryopvp.arenapvp.arena;

import com.embryopvp.arenapvp.ArenaPVP;
import com.embryopvp.arenapvp.config.ConfigManager;
import com.embryopvp.arenapvp.utils.LogUtil;

import com.embryopvp.arenapvp.exceptions.ArenaStateNotChangedException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import javax.annotation.Nonnull;
import java.io.*;

public abstract class Arena {

    protected String arenaID = "INVALID";
    private boolean enabled = false;
    private boolean FFA = false;
    private FileConfiguration arenaConfig;
    private File arenaFile;
    private String name;
    private World world;
    private Location FFASpawn;

    public Arena(boolean enabled) {
        this.enabled = enabled;
    }

    public abstract void enableArena();

    public abstract void disableArena();

    protected final void setArenaID(String featureID) {
        this.arenaID = featureID;
    }

    public final String getArenaID() {
        return this.arenaID;
    }

    public final boolean isEnabled() {
        return this.enabled;
    }

    public final void setEnabled(boolean enabled) throws ArenaStateNotChangedException{

        if (isEnabled() != enabled) {

            if (enabled) {

                this.enabled = true;
                enableArena();

            } else {

                this.enabled = false;
                disableArena();

            }

        } else {
            throw new ArenaStateNotChangedException();
        }

    }

    public final boolean isFFA() {
        return this.FFA;
    }

    public final void setFFA(boolean ffaArena) throws ArenaStateNotChangedException{

        if (isFFA() != ffaArena) {

            if (ffaArena) {

                this.FFA = true;

            } else {

                this.FFA = false;

            }

        } else {
            throw new ArenaStateNotChangedException();
        }

    }

    public final void setName(String name) {
        this.name = name;
    }

    public final void setWorld(String worldName) {
        this.world = Bukkit.getWorld(worldName);
    }

    public final String getName() {
        return this.name;
    }

    public final void setArenaConfig(FileConfiguration arenaConfig) {
        this.arenaConfig = arenaConfig;
    }

    public final FileConfiguration getArenaConfig() {
        return this.arenaConfig;
    }

    public final void setArenaFile(File arenaFile) {
        this.arenaFile = arenaFile;
    }

    public final File getArenaFile() {
        return this.arenaFile;
    }


    public Location getFFASpawn() {
        return new Location(world, arenaConfig.getDouble("arena.ffa-spawn.x"), arenaConfig.getDouble("arena.ffa-spawn.y"), arenaConfig.getDouble("arena.ffa-spawn.z"));
    }

    public boolean equals(Object arenaObject) {
        return ((arenaObject instanceof Arena) && (((Arena)arenaObject).getArenaID().equals(getArenaID())));
    }

    public final void saveArenaConfig()
    {
        try {
            arenaConfig.save(arenaFile);
        } catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

}
