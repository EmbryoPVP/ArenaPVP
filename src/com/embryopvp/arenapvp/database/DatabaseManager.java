package com.embryopvp.arenapvp.database;

import com.embryopvp.arenapvp.ArenaPVP;
import com.embryopvp.arenapvp.config.ConfigManager;
import com.embryopvp.arenapvp.utils.LogUtil;


public class DatabaseManager {

    private static DatabaseManager instance = new DatabaseManager();
    protected static boolean run = true;
    private static DatabaseThread thread = null;
    public static final String tablePrefix = ConfigManager.getInstance().getConfig().getString("database.settings.prefix");

    public DatabaseManager() {

    }

    public static DatabaseManager getInstance() {
        return instance;
    }

    public void open() {
        String type = ConfigManager.getInstance().getConfig().getString("database.type");

        if(type == null) {
            LogUtil.LogToConsole("Can't parse database type. Using SQLite");
            type = "SQLite";
        }

        open(type);
    }

    protected void open(String type) {
        DatabaseCore core = null;
        if(type.equals("SQLite")) {
            core = new SQLite(ArenaPVP.getPlugin().getDataFolder().getPath());
        } else if(type.equals("MySQL")) {
            core = new MySQL(ConfigManager.getInstance().getConfig().getString("database.settings.host"),
                    ConfigManager.getInstance().getConfig().getInt("database.settings.port"),
                    ConfigManager.getInstance().getConfig().getString("database.settings.dbname"), ConfigManager.getInstance().getConfig().getString("database.settings.username"),
                    ConfigManager.getInstance().getConfig().getString("database.settings.password"));
        } else {
            LogUtil.LogToConsole("Can't parse data for database type \"" + type + "\". Using SQLite");
            open("SQLite");
            return;
        }
        thread = new DatabaseThread(core);
        thread.start();
    }

    public static void close() {
        run = false;
        thread = null;
    }

    // OTHER STUFF

    public void setupDatabase() {
        DatabaseThread.addTask(new DatabaseTask("CREATE TABLE IF NOT EXISTS `" + tablePrefix + "uhcffa_stats` (" +
                " `uuid` varchar(36) NOT NULL," +
                " `username` varchar(255) NOT NULL," +
                " `deaths` int(100) default '0'," +
                " `kills` int(100) default '0');"));
    }

    public static String getTablePrefix() {
        return tablePrefix;
    }

}
