package com.embryopvp.arenapvp.database;

import java.sql.ResultSet;

public interface DatabaseResponse {

    public void response(DatabaseTask task, ResultSet rs);

}
